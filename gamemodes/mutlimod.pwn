#include <a_samp>
#include <a_mysql>
#include <accounts>
#include <other>

// Postavke modas
#define gamemode_version "v0.1"
#define gamemode_map "SA"

// MySQL
forward WhenAccountCheck(playerid, password[]);

// Colors
#define COLOR_ADMIN_INFO 0x5CAD5CFF
#define COLOR_ERROR 0x5CAD5CFF
#define COLOR_FADE1 0xE6E6E6E6
#define COLOR_FADE2 0xC8C8C8C8
#define COLOR_FADE3 0xAAAAAAAA
#define COLOR_FADE4 0x8C8C8C8C
#define COLOR_FADE5 0x6E6E6E6E
#define COLOR_GRAD 0xBFC0C2FF
#define COLOR_GREEN 0x33AA33AA
#define COLOR_YELLOW2 0xF5DEB3AA
#define COLOR_YELLOW3 0xFFD633AA
#define COLOR_SERVER_GREEN 0x21DD00FF
#define COLOR_LIGHTBLUE 0x33CCFFAA
#define COLOR_LIGHTRED 0xFF6347AA
#define COLOR_LIGHTRED2 0xE63E3EFF
#define COLOR_SYN 0xB8DBFFFF
#define COLOR_SYN2 0xA9C4E4FF
#define COLOR_WHITE 0xFFFFFFFF
//

// Faction colors
#define COLOR_LSPD 0x2641FEAA
#define COLOR_LVPD 0x2641FEAA
#define COLOR_SFPD 0x2641FEAA
#define COLOR_FBI 0x2641FEAA
#define COLOR_NG 0x1E519DAA
#define COLOR_PARAMEDICS 0xFF6347AA

#define COLOR_FACTION_CHAT 0x01FCFFC8
#define COLOR_RADIO_CHAT 0x8D8DFF00
//

// Dialogs
#define DIALOG_LOGIN 1
#define DIALOG_REGISTER 2
#define DIALOG_SEX 3
#define DIALOG_AGE 4
#define DIALOG_EMAIL 5
#define DIALOG_REGISTER_FINISH 6
//

// Login
new gRegistred[MAX_PLAYERS];
new gLogged[MAX_PLAYERS];

new RegistrationStep[MAX_PLAYERS];

forward OnPlayerRegister(playerid, password[]);
forward OnPlayerLogin(playerid, password[]);
forward WhenPlayerLogin(playerid);
//

// Money
#define ResetMoneyBar ResetPlayerMoney
#define UpdateMoneyBar GivePlayerMoney

new Cash[MAX_PLAYERS];
//

// Tutorial
new TutorialTime[MAX_PLAYERS];
//

// Clock
new clockupdate;

new Text:ClockTime;
new Text:ClockDate;

forward ClockUpdate();
//

// Admin & Helpers & Reports
#define ADMIN_SPEC_TYPE_NONE 0
#define ADMIN_SPEC_TYPE_PLAYER 1
#define ADMIN_SPEC_TYPE_VEHICLE 2

new ReportTime[MAX_PLAYERS];
new SpectateType[MAX_PLAYERS];
new SpectatedID[MAX_PLAYERS];

forward AdminsBroadCast(color, string[]);
forward HelpersBroadCast(color, string[]);

forward ReportTimer(playerid);
//

// Faction
forward SetPlayerFactionColor(playerid);
forward FactionsBroadCast(faction, color, string[]);
forward RadioBroadCast(faction, color, string[]);
//

// Timers
new othertimers;
forward OtherTimers();
//

//

forward ShowStats(playerid, targetid);
forward KickPublic(playerid);

forward ProxDetector(Float:radi, playerid, string[],col1,col2,col3,col4,col5);

forward MoneyTimer();

main()
{
	printf(" ");
	printf(" ");
	printf(" RPG: %s %s", gamemode_version, gamemode_map);
	printf("_____________________");
}

public OnGameModeInit()
{
	// MySQL
	MySQLConnect();
	mysql_log(LOG_DEBUG | LOG_ERROR | LOG_WARNING,LOG_TYPE_HTML);
	//
	
	// Cars
	AddStaticVehicleEx (411, 1741.7678, -1852.7793, 13.4141, 267.4383, 0, 1, 60);

	// Gamemode and Map text
	new string [500];
	SetGameModeText(#gamemode_version);
	format(string, sizeof(string), "mapname %s", gamemode_map);
	SendRconCommand(string);

	// Timers
	SetTimer("MoneyTimer", 1000, true);
	othertimers = SetTimer("OtherTimers", 1000, true);

	// Clock
	clockupdate = SetTimer("ClockUpdate", 1000, 1);

	ClockTime = TextDrawCreate(548.000000,21.000000,"00:00");
 	TextDrawAlignment(ClockTime,1);
    TextDrawFont(ClockTime,3);
    TextDrawUseBox(ClockTime, 0);
    TextDrawSetOutline(ClockTime,2);
    TextDrawColor(ClockTime,0xffffffff);
    TextDrawSetShadow(ClockTime,1);
    TextDrawLetterSize(ClockTime,0.589999,2.300000);

	ClockDate = TextDrawCreate(535.000000,6.000000,"00/00/0000");
 	TextDrawAlignment(ClockDate,1);
    TextDrawFont(ClockDate,1);
    TextDrawUseBox(ClockDate, 0);
    TextDrawSetOutline(ClockDate,1);
    TextDrawColor(ClockDate,0xffffffff);
    TextDrawSetShadow(ClockDate,1);
    TextDrawLetterSize(ClockDate,0.450000,1.400000);
	//

    ShowPlayerMarkers(0);
    EnableStuntBonusForAll(0);
    UsePlayerPedAnims();

	return 1;
}

public OnGameModeExit()
{
	KillTimer(othertimers);
	KillTimer(clockupdate);
    mysql_close();
	return 1;
}

public OnPlayerRequestClass(playerid, classid)
{
	return 1;
}

public OnPlayerConnect(playerid)
{
    gRegistred[playerid] = 0; gLogged[playerid] = 0;
    RegistrationStep[playerid] = 0; TutorialTime[playerid] = 0;
    ReportTime[playerid] = 0; SpectatedID[playerid] = 0;

    PlayerInfo[playerid][pRegistred] = 0;
    PlayerInfo[playerid][pTutorial] = 0;
    PlayerInfo[playerid][pSex] = 0;

	new pName[30];
	new query[256];
	GetPlayerName(playerid, pName, sizeof(pName));
  	mysql_format(handle, query, sizeof(query), "SELECT * FROM `players` WHERE `username` = '%e'", pName);
	mysql_tquery(handle, query, "WhenAccountCheck", "i", playerid);

	TogglePlayerSpectating(playerid, 0);

	TextDrawShowForPlayer(playerid, ClockTime);
	TextDrawShowForPlayer(playerid, ClockDate);

	SetPlayerColor(playerid, COLOR_GRAD);

	return 1;
}

public WhenAccountCheck(playerid, password[])
{
    new pName[30];
	GetPlayerName(playerid, pName, sizeof(pName));

    cache_get_data(rows, fields, handle);

 	if(rows)
	{
		gRegistred[playerid] = 1;
  	}
  	else
  	{
  	    gRegistred[playerid] = 0;
	}

	// Login or Register!

  	if(gLogged[playerid] == 0)
	{
	    if(gRegistred[playerid] == 1)
	    {
			if(rows)
			{
			    new string[256];
   				GetPlayerName(playerid, pName, sizeof(pName));

   				cache_get_field_content(0, "LastLogin", temp), strmid(PlayerInfo[playerid][pLastLogin], temp, 0, 255, 255);

	         	format(string,sizeof(string),"{BECBFC}Welcome, {95A3FF}%s{BECBFC}!\n\nPlease enter your password below to login to your account.\n\n* {FFFFFF}Last login: %s *",pName, PlayerInfo[playerid][pLastLogin]);
				ShowPlayerDialog(playerid, DIALOG_LOGIN ,DIALOG_STYLE_PASSWORD,"Login",string,"Login","Exit");
   			}
     	}
     	else
     	{
     	    ShowPlayerDialog(playerid, DIALOG_REGISTER, DIALOG_STYLE_PASSWORD, "Register", "Enter your password below:", "Register", "Cancel");
   		}
	}

    SetPlayerCameraPos(playerid, 1500.7156,-1778.1880,80.3080);
	SetPlayerCameraLookAt(playerid, 1500.7156,-1778.1880,80.3080);
	SetPlayerPos(playerid, 1437.6957,-2266.6355,13.5469);

	return 1;
}

public OnPlayerRegister(playerid, password[])
{
    if(IsPlayerConnected(playerid))
	{
		new pName[30];
		new query[256];
		new string[256];
		GetPlayerName(playerid, pName, sizeof(pName));

        mysql_format(handle, query, sizeof(query), "INSERT INTO `players` (`username`,`password`) VALUES ('%e', MD5('%e'))", pName, password);
        mysql_tquery(handle, query, "", "");

        strmid(PlayerInfo[playerid][pPassword], password, 0, strlen(password), 255);

        format(string, sizeof(string), "Please enter your password below to login to your account.", pName);
		ShowPlayerDialog(playerid, DIALOG_LOGIN ,DIALOG_STYLE_PASSWORD, "Login", string, "Login", "Exit");

	}
	return 1;
}

public OnPlayerLogin(playerid, password[])
{
    if(IsPlayerConnected(playerid))
	{
		new pName[30];
		new query[256];
		GetPlayerName(playerid, pName, sizeof(pName));

        mysql_format(handle, query, sizeof(query), "SELECT * FROM `players` WHERE `username`='%e' AND `password`=MD5('%e')", pName, password);
        mysql_tquery(handle, query, "WhenPlayerLogin", "i", playerid);

	}

	TogglePlayerSpectating(playerid, 0);

	return 1;
}

public WhenPlayerLogin(playerid)
{
    if(IsPlayerConnected(playerid))
	{
	    new string[256];
	    new pName[30];
		GetPlayerName(playerid, pName, sizeof(pName));

	    cache_get_data(rows, fields, handle);

		if(rows != 0)
		{
		    cache_get_field_content(0, "password", temp),  format(PlayerInfo[playerid][pPassword], 128, temp);
			cache_get_field_content(0, "Level", temp), PlayerInfo[playerid][pLevel] = strval(temp);
			cache_get_field_content(0, "AdminLevel", temp), PlayerInfo[playerid][pAdmin] = strval(temp);
			cache_get_field_content(0, "HelperLevel", temp), PlayerInfo[playerid][pHelper] = strval(temp);
			cache_get_field_content(0, "Cash", temp), PlayerInfo[playerid][pCash] = strval(temp);
			cache_get_field_content(0, "Account", temp), PlayerInfo[playerid][pAccount] = strval(temp);
			cache_get_field_content(0, "Email", temp),  format(PlayerInfo[playerid][pEmail], 256, temp);
			cache_get_field_content(0, "Registred", temp), PlayerInfo[playerid][pRegistred] = strval(temp);
			cache_get_field_content(0, "Tutorial", temp), PlayerInfo[playerid][pTutorial] = strval(temp);
			cache_get_field_content(0, "Sex", temp), PlayerInfo[playerid][pSex] = strval(temp);
			cache_get_field_content(0, "Age", temp), PlayerInfo[playerid][pAge] = strval(temp);
			cache_get_field_content(0, "PhoneNumber", temp), PlayerInfo[playerid][pPhoneNumber] = strval(temp);
			cache_get_field_content(0, "PremiumAccount", temp), PlayerInfo[playerid][pPremiumAccount] = strval(temp);
			cache_get_field_content(0, "Banned", temp), PlayerInfo[playerid][pBanned] = strval(temp);
			cache_get_field_content(0, "Warns", temp), PlayerInfo[playerid][pWarns] = strval(temp);
			cache_get_field_content(0, "Leader", temp), PlayerInfo[playerid][pLeader] = strval(temp);
			cache_get_field_content(0, "Member", temp), PlayerInfo[playerid][pMember] = strval(temp);
			cache_get_field_content(0, "Rank", temp), PlayerInfo[playerid][pRank] = strval(temp);
			cache_get_field_content(0, "Skin", temp), PlayerInfo[playerid][pSkin] = strval(temp);
			cache_get_field_content(0, "IP", temp), PlayerInfo[playerid][pIP] = strval(temp);
		}
		else
		{
			ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "Login", "The passwords don't match!\nType your account's password below:", "Login", "Exit");
	        return 1;
		}

		if(PlayerInfo[playerid][pRegistred] == 0)
		{
            PlayerInfo[playerid][pLevel] = 1;
            Update(playerid, pLevelu);

            PlayerInfo[playerid][pAdmin] = 0;
            Update(playerid, pAdminu);

            GivePlayerCash(playerid, 150);

            PlayerInfo[playerid][pAccount] = 500;
            Update(playerid, pAccountu);

            PlayerInfo[playerid][pRegistred] = 1;
			Update(playerid, pRegistredu);

			new randomphonenumber = 1000000 + random(9999999);
			PlayerInfo[playerid][pPhoneNumber] = randomphonenumber;
			Update(playerid, pPhoneNumberu);

			PlayerInfo[playerid][pInterior] = 0;
  		}

        if(PlayerInfo[playerid][pBanned] == 1)
		{
		    KickWithMessage(playerid, COLOR_LIGHTRED2, "Your account is currently banned.");
		    KickWithMessage(playerid, COLOR_LIGHTRED2, "In case you think this is a confusion, request an unban at the forums.");
		}

        GetPlayerCash(playerid);

		SetPlayerScore(playerid, PlayerInfo[playerid][pLevel]);
  		SetPlayerCash(playerid, PlayerInfo[playerid][pCash]);

		SetSpawnInfo(playerid, 0, 0, 1742.9515, -1862.3721, 13.5764, 359.5591, 0, 0, 0, 0, 0, 0);
		SpawnPlayer(playerid);

		Update(playerid, pLastLoginu);

		new pNameIP[256];
		GetPlayerIp(playerid, pNameIP, sizeof(pNameIP));

		strmid(PlayerInfo[playerid][pIP], pNameIP, 0, 100, 255);
		Update(playerid, pIPu);


		if (PlayerInfo[playerid][pAdmin] >= 1)
		{
			format(string, sizeof(string), "You're logged in as a level %d admin.", PlayerInfo[playerid][pAdmin]);
			SendClientMessage(playerid, COLOR_YELLOW2, string);
		}

		if (PlayerInfo[playerid][pHelper] >= 1)
		{
			format(string, sizeof(string), "You're logged in as a level %d helper.", PlayerInfo[playerid][pHelper]);
			SendClientMessage(playerid, COLOR_YELLOW2, string);
		}

		if (PlayerInfo[playerid][pPremiumAccount] >= 1)
		{
			SendClientMessage(playerid, COLOR_WHITE,"You're a premium account user. ");
		}

		printf("%s has logged in.", pName);

		gLogged[playerid] = 1;
	}

	return 1;
}

public OnPlayerDisconnect(playerid, reason)
{
	return 1;
}

public OnPlayerSpawn(playerid)
{
    if(IsPlayerConnected(playerid))
	{
	    SetPlayerFactionColor(playerid);
	    if(gLogged[playerid] == 0)
		{
	    	KickWithMessage(playerid, COLOR_LIGHTRED, "** This server requires a Login BEFORE spawn (Kicked) **");
	 	}

        if(PlayerInfo[playerid][pSkin] > 0)
		{
			SetPlayerSkin(playerid, PlayerInfo[playerid][pSkin]);
		}
		else
		{
			SetPlayerSkin(playerid, PlayerInfo[playerid][pSkin]);
		}

	    if(PlayerInfo[playerid][pTutorial] == 0)
	    {
	        SetPlayerCameraPos(playerid, 1500.7156,-1778.1880,80.3080);
			SetPlayerCameraLookAt(playerid, 1500.7156,-1778.1880,80.3080);
			SetPlayerPos(playerid, 1437.6957,-2266.6355,13.5469);

			ShowPlayerDialog(playerid, DIALOG_SEX, DIALOG_STYLE_MSGBOX, "Character Gender", "Are you a male or a female?", "Male", "Female");
		}

		if (PlayerInfo[playerid][pMember] == 1 || PlayerInfo[playerid][pLeader] == 1)// Los Santos Police Departament spawn
		{
			SetPlayerFactionColor(playerid);

			PlayerInfo[playerid][pInterior] = 6;
			SetPlayerInterior(playerid, 6);

			SetPlayerPos(playerid, 216.9725,79.1339,1005.0391);
			SetPlayerFacingAngle(playerid, 0);
			return 1;
		}
		if (PlayerInfo[playerid][pMember] == 2 || PlayerInfo[playerid][pLeader] == 2)// Las Venturas Police Departament spawn
		{
			SetPlayerFactionColor(playerid);

			PlayerInfo[playerid][pInterior] = 3;
			SetPlayerInterior(playerid, 3);

			SetPlayerPos(playerid, 297.3488,186.3873,1007.1719);
			SetPlayerFacingAngle(playerid, 0);
			return 1;
		}
		if (PlayerInfo[playerid][pMember] == 3 || PlayerInfo[playerid][pLeader] == 3)// San Fierro Police Departament spawn
		{
			SetPlayerFactionColor(playerid);

			PlayerInfo[playerid][pInterior] = 0;
			SetPlayerInterior(playerid, 0);

			SetPlayerPos(playerid, -1606.3876,676.6782,-5.2422);
			SetPlayerFacingAngle(playerid, 0);
			return 1;
		}
		if (PlayerInfo[playerid][pMember] == 4 || PlayerInfo[playerid][pLeader] == 4)// FBI spawn
		{
			SetPlayerFactionColor(playerid);

			PlayerInfo[playerid][pInterior] = 10;
			SetPlayerInterior(playerid, 0);

			SetPlayerPos(playerid, 246.375991,109.245994,1003.218750);
			SetPlayerFacingAngle(playerid, 90);
			return 1;
		}
  		if (PlayerInfo[playerid][pMember] == 5 || PlayerInfo[playerid][pLeader] == 5)// National Guard spawn
		{
			SetPlayerFactionColor(playerid);

			PlayerInfo[playerid][pInterior] = 0;
			SetPlayerInterior(playerid, 0);

			SetPlayerPos(playerid, 246.6305,1860.1299,14.0840);
			SetPlayerFacingAngle(playerid, 0);
			return 1;
		}
		if (PlayerInfo[playerid][pMember] == 6 || PlayerInfo[playerid][pLeader] == 6)// Paramedics spawn
		{
			SetPlayerFactionColor(playerid);

			PlayerInfo[playerid][pInterior] = 0;
			SetPlayerInterior(playerid, 0);

			SetPlayerPos(playerid, 1607.1846,1818.9738,10.8203);
			SetPlayerFacingAngle(playerid, 0);
			return 1;
		}
	}
	return 1;
}

public OnPlayerDeath(playerid, killerid, reason)
{
	return 1;
}

public OnVehicleSpawn(vehicleid)
{
	return 1;
}

public OnVehicleDeath(vehicleid, killerid)
{
	return 1;
}

public OnPlayerText(playerid, text[])
{
    new pName[MAX_PLAYER_NAME];
	new string[256];
 	GetPlayerName(playerid, pName, sizeof(pName));

    format(string, sizeof(string), "%s says: %s", pName, text);
	ProxDetector(20.0, playerid, string, COLOR_FADE1, COLOR_FADE2, COLOR_FADE3, COLOR_FADE4, COLOR_FADE5);
	return 0;
}

public OnPlayerCommandText(playerid, cmdtext[])
{
    new cmd[256], idx, string[256];
    cmd = strtok(cmdtext, idx);

	new pName[MAX_PLAYER_NAME];
	new targetid;
	new pNameGive[MAX_PLAYER_NAME];

	new Float:x, Float:y, Float:z;

	// General Commands
	if(strcmp(cmd, "/help", true) == 0)
	{
		if(IsPlayerConnected(playerid))
		{
		    if(gLogged[playerid] == 1)
		    {
			    SendClientMessage(playerid, COLOR_WHITE, "*** ACCOUNT *** /stats");
		    	SendClientMessage(playerid, COLOR_WHITE, "*** CHAT *** -");
		    	SendClientMessage(playerid, COLOR_WHITE, "*** GENERAL *** /report /helpme");
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
  		}

		return 1;
	}

  	if(strcmp(cmd, "/stats", true) == 0)
	{
		if(IsPlayerConnected(playerid))
		{
		    GetPlayerName(playerid, pName, sizeof(pName));

		    if(gLogged[playerid] == 1)
		    {
		    	ShowStats(playerid, playerid);
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
		return 1;
	}

	if(strcmp(cmd, "/report", true) == 0)
    {
        if(IsPlayerConnected(playerid))
		{
		    if(gLogged[playerid] == 1)
		    {
			    GetPlayerName(playerid, pName, sizeof(pName));

				new length = strlen(cmdtext);

				while ((idx < length) && (cmdtext[idx] <= ' '))
				{
					idx++;
				}

				new offset = idx;
				new result[128];

				while ((idx < length) && ((idx - offset) < (sizeof(result) - 1)))
				{
					result[idx - offset] = cmdtext[idx];
					idx++;
				}

				result[idx - offset] = EOS;

				if(!strlen(result))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: /report <text>");
					return 1;
				}

				if(ReportTime[playerid] == 0)
				{
		  			format(string, sizeof(string), "Report from %s (ID:%d): %s", pName, playerid, result);
		    		AdminsBroadCast(COLOR_LIGHTRED2, string);

		    		SendClientMessage(playerid, COLOR_YELLOW2, "Your report got sent to the administrator!");

		    		ReportTime[playerid] = 10;
		    		SetTimerEx("ReportTimer", 10000, false, "i", playerid);
				}

				else
				{
				    format(string, sizeof(string), "You need to wait %d seconds before send another report.",  ReportTime[playerid]);
	                SendClientMessage(playerid, COLOR_YELLOW2, string);
	       		}
	        }
	        else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
    	return 1;
    }

    if(strcmp(cmd, "/helpme", true) == 0)
    {
        if(IsPlayerConnected(playerid))
		{
		    if(gLogged[playerid] == 1)
		    {
			    GetPlayerName(playerid, pName, sizeof(pName));

				new length = strlen(cmdtext);

				while ((idx < length) && (cmdtext[idx] <= ' '))
				{
					idx++;
				}

				new offset = idx;
				new result[128];

				while ((idx < length) && ((idx - offset) < (sizeof(result) - 1)))
				{
					result[idx - offset] = cmdtext[idx];
					idx++;
				}

				result[idx - offset] = EOS;

				if(!strlen(result))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: /helpme <text>");
					return 1;
				}

				if(ReportTime[playerid] == 0)
				{
		  			format(string, sizeof(string), "Report from %s (ID:%d): %s", pName, playerid, result);
		    		AdminsBroadCast(COLOR_LIGHTRED2, string);

		    		SendClientMessage(playerid, COLOR_YELLOW2, "Your help request got sent to the helpers!");

		    		ReportTime[playerid] = 10;
		    		SetTimerEx("ReportTimer", 10000, false, "i", playerid);
				}

				else
				{
				    format(string, sizeof(string), "You need to wait %d seconds before request another help.",  ReportTime[playerid]);
	                SendClientMessage(playerid, COLOR_YELLOW2, string);
	       		}
	        }
	        else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
        }
    	return 1;
    }

	//
	// Admin Commands
    if(strcmp(cmd, "/ah", true) == 0 || strcmp(cmd, "/adminhelp", true) == 0)
	{
		if(IsPlayerConnected(playerid))
		{
		    if(gLogged[playerid] == 1)
		    {
			    if(PlayerInfo[playerid][pAdmin] >= 1)
				{
				    SendClientMessage(playerid, COLOR_GREEN, "_______________________________________");
				    if(PlayerInfo[playerid][pAdmin] >= 1)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Admin level 1 ** (/ah)dminhelp (/a)dmin /goto /gethere /respawn /ban /kick (/checkw)eapons /check /spec (/rcon)");
				    }
				    if(PlayerInfo[playerid][pAdmin] >= 2)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Admin level 2 ** (/bano)ffline**");
				    }
				    if(PlayerInfo[playerid][pAdmin] >= 3)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Admin level 3 **");
				    }
				    if(PlayerInfo[playerid][pAdmin] >= 4)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Admin level 4 **");
				    }
				    if(PlayerInfo[playerid][pAdmin] >= 5)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Admin level 5 **");
				    }
				    if(PlayerInfo[playerid][pAdmin] >= 6)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Admin level 6 ** /makeadmin /makehelper /makeleader /auninvite");
				    }
				    SendClientMessage(playerid, COLOR_GREEN, "_______________________________________");
	   			}

				else if(PlayerInfo[playerid][pAdmin] < 1)
				{
					SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
				}
	  		}
	  		else
			{
		    	SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
  			}
		}
		return 1;
	}

    if(strcmp(cmd, "/a", true) == 0 || strcmp(cmd, "/adminchat", true) == 0)
    {
        if(IsPlayerConnected(playerid))
		{
		    if(gLogged[playerid] == 1)
		    {
		        GetPlayerName(playerid, pName, sizeof(pName));

				new length = strlen(cmdtext);

				while ((idx < length) && (cmdtext[idx] <= ' '))
				{
					idx++;
				}

				new offset = idx;
				new result[128];

				while ((idx < length) && ((idx - offset) < (sizeof(result) - 1)))
				{
					result[idx - offset] = cmdtext[idx];
					idx++;
				}

				result[idx - offset] = EOS;

				if(!strlen(result))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: (/a)dminchat <text>");
					return 1;
				}

		        if (PlayerInfo[playerid][pAdmin] >= 1 && PlayerInfo[playerid][pAdmin] <= 5)
		        {
		        	format(string, sizeof(string), "** Admin Level %d %s : %s **", PlayerInfo[playerid][pAdmin], pName, result);
		        	AdminsBroadCast(COLOR_YELLOW2, string);
		        }

		        if (PlayerInfo[playerid][pAdmin] == 6)
		        {
		        	format(string, sizeof(string), "{CFAE70}** Admin Level %d %s : %s **", PlayerInfo[playerid][pAdmin], pName, result);
		        	AdminsBroadCast(COLOR_WHITE, string);
		        }

		        if (PlayerInfo[playerid][pAdmin] < 1)
				{
	       			SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
			    	return 1;
	  		    }
	        }
	        else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
    	return 1;
    }

	if(strcmp(cmd, "/ban", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(gLogged[playerid] == 1)
	        {
	            GetPlayerName(playerid, pName, sizeof(pName));

	            temp = strtok(cmdtext, idx);

	            if(!strlen(temp))
	            {
	                SendClientMessage(playerid, COLOR_SYN, "Synthax: /ban <playerid/PartOfName> <reason>");
					return 1;
				}

				targetid = ReturnUser(temp);

				if (PlayerInfo[playerid][pAdmin] >= 1)
				{
				    if(IsPlayerConnected(targetid))
			    	{
			    	    if(targetid != INVALID_PLAYER_ID)
			        	{
			        		GetPlayerName(targetid, pNameGive, sizeof(pNameGive));

			        		new length = strlen(cmdtext);

							while ((idx < length) && (cmdtext[idx] <= ' '))
							{
								idx++;
							}

							new offset = idx;
							new result[128];

							while ((idx < length) && ((idx - offset) < (sizeof(result) - 1)))
							{
								result[idx - offset] = cmdtext[idx];
								idx++;
							}

							result[idx - offset] = EOS;

							if(!strlen(result))
							{
								SendClientMessage(playerid, COLOR_SYN, "Synthax: /ban <playerid/PartOfName> <reason>");
								return 1;
							}

							if(PlayerInfo[targetid][pAdmin] > 0 && PlayerInfo[playerid][pAdmin] < 6)
				            {
								SendClientMessage(playerid, COLOR_ERROR, "AdmWarning: You can't ban admins!");
								format(string, sizeof(string), "AdmCmd: %s tried to ban admin %s with the reason: %s", pName, pNameGive, (result));
								AdminsBroadCast(COLOR_LIGHTRED, string);
								return 1;
				            }

				            format(string, sizeof(string), "AdmCmd: %s was banned by %s, reason: %s", pNameGive, pName, (result));
							SendClientMessageToAll(COLOR_LIGHTRED, string);
							PlayerInfo[targetid][pBanned] = 1;
							Update(targetid, pBannedu);

							new pGiveIP[256];
							GetPlayerIp(targetid, pGiveIP, sizeof(pGiveIP));
							strmid(PlayerInfo[targetid][pIP], pGiveIP, 0, 100, 255);
							Update(targetid, pIPu);

							new query[512];
							mysql_format(handle, query, sizeof(query) ,"INSERT INTO `banslogs` (`ID`,`username`,`IP`,`Admin`,`Reason`,`Type`,`Date`,`Time`) VALUES ('','%e','%e','%e','%e','%e',CURRENT_TIMESTAMP,'%d')",
							pNameGive, PlayerInfo[targetid][pIP], pName, result, "Online", 0);
							mysql_query(handle, query);

							Kick(targetid);
							return 1;
						}
					}
					else
					{
						SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername.");
					}
				}
				else
				{
					SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
				}
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
		return 1;
	}

	if(strcmp(cmd, "/bano", true) == 0 || strcmp(cmd, "/banoffline", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(gLogged[playerid] == 1)
	        {
	            cache_get_data(rows, fields, handle);

				new query[512];
				mysql_format(handle, query, sizeof(query), "SELECT `username` FROM `players` WHERE `username` = '%e' AND `Banned` = '0' LIMIT 0", temp);
				mysql_query(handle, query);

	            GetPlayerName(playerid, pName, sizeof(pName));

	            temp = strtok(cmdtext, idx);

	            if(!strlen(temp))
	            {
	                SendClientMessage(playerid, COLOR_SYN, "Synthax: (/bano)ffline <playername> <reason>");
					return 1;
				}

				if (PlayerInfo[playerid][pAdmin] >= 2)
				{
					if(!rows)
					{
						format(string, sizeof(string), "Error: Nobody with the name %s is registered or this player is alredy banned.");
                        return SendClientMessage(playerid, COLOR_ERROR, string);
					}

				    else if(rows)
			    	{
      					GetPlayerName(targetid, pNameGive, sizeof(pNameGive));

						new length = strlen(cmdtext);

      					while ((idx < length) && (cmdtext[idx] <= ' '))
      					{
      						idx++;
      					}

      					new offset = idx;
      					new result[128];

      					while ((idx < length) && ((idx - offset) < (sizeof(result) - 1)))
      					{
      						result[idx - offset] = cmdtext[idx];
      						idx++;
      					}

      					result[idx - offset] = EOS;

      					if(!strlen(result))
      					{
      						SendClientMessage(playerid, COLOR_SYN, "Synthax: (/bano)ffline <playername> <reason>");
      						return 1;
      					}

      					if(PlayerInfo[targetid][pAdmin] > 0 && PlayerInfo[playerid][pAdmin] < 6)
						{
      						SendClientMessage(playerid, COLOR_ERROR, "AdmWarning: You can't ban admins!");
      						format(string, sizeof(string), "AdmCmd: %s tried to ban admin %s with the reason: %s", pName, pNameGive, (result));
      						AdminsBroadCast(COLOR_LIGHTRED, string);
      						return 1;
						}

						format(string, sizeof(string), "AdmCmd: %s was banned offline by %s, reason: %s", temp, pName, (result));
      					SendClientMessageToAll(COLOR_LIGHTRED, string);

           				mysql_format(handle, query, sizeof(query), "UPDATE `players` SET `Banned` = '1' WHERE username = '%e'", temp);
						mysql_query(handle, query);

      					mysql_format(handle, query, sizeof(query) ,"INSERT INTO `banslogs` (`ID`,`username`,`IP`,`Admin`,`Reason`,`Type`,`Date`,`Time`) VALUES ('','%e','%e','%e','%e','%e',CURRENT_TIMESTAMP,'%d')",
      					temp, PlayerInfo[targetid][pIP], pName, result, "Offline", 0);
      					mysql_query(handle, query);

                        new pGiveIP[256];
						GetPlayerIp(targetid, pGiveIP, sizeof(pGiveIP));
						strmid(PlayerInfo[targetid][pIP], pGiveIP, 0, 100, 255);

						return 1;
					}
				}
				else
				{
					SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
				}
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
		return 1;
	}

    if(strcmp(cmd, "/kick", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(gLogged[playerid] == 1)
	        {
		    	temp = strtok(cmdtext, idx);
				if(!strlen(temp))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: /kick <playerid/PartOfName> <reason>");
					return 1;
				}
				targetid = ReturnUser(temp);
				if (PlayerInfo[playerid][pAdmin] >= 1)
				{
					if(IsPlayerConnected(targetid))
					{
					    if(targetid != INVALID_PLAYER_ID)
					    {
						    GetPlayerName(targetid, pNameGive, sizeof(pNameGive));
							GetPlayerName(playerid, pName, sizeof(pName));
							new length = strlen(cmdtext);
							while ((idx < length) && (cmdtext[idx] <= ' '))
							{
								idx++;
							}
							new offset = idx;
							new result[128];
							while ((idx < length) && ((idx - offset) < (sizeof(result) - 1)))
							{
								result[idx - offset] = cmdtext[idx];
								idx++;
							}
							result[idx - offset] = EOS;
							if(!strlen(result))
							{
								SendClientMessage(playerid, COLOR_SYN, "Synthax: /kick <playerid/PartOfName> <reason>");
								return 1;
							}
							if (PlayerInfo[targetid][pAdmin] > PlayerInfo[playerid][pAdmin])
							{
							    SendClientMessage(playerid, COLOR_ERROR, "Error: You cannot kick an administrator higher than your level.");
							    return 1;
							}

							format(string, sizeof(string), "AdmCmd:  %s got kicked by %s, reason: %s", pNameGive, pName, (result));
							SendClientMessageToAll(COLOR_LIGHTRED, string);

                            new query[512];
							mysql_format(handle, query, sizeof(query) ,"INSERT INTO `kickslogs` (`ID`,`username`,`IP`,`Admin`,`Reason`,`Date`) VALUES ('','%e','%e','%e','%e',CURRENT_TIMESTAMP)",
							pNameGive, PlayerInfo[targetid][pIP], pName, result);
							mysql_query(handle, query);

	   						Kick(targetid);
							return 1;
						}
					}
					else
					{
						SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername.");
					}
				}
				else
				{
					SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
				}
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
		return 1;
	}

    if(strcmp(cmd, "/makeadmin", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
         	if(gLogged[playerid] == 1)
		    {
				temp = strtok(cmdtext, idx);

				if(!strlen(temp))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: /makeadmin <playerid/PartOfName> <admin level (1-6)>");
					return 1;
				}
				new adminlevel;

				targetid = ReturnUser(temp);
				temp = strtok(cmdtext, idx);
				adminlevel = strval(temp);

				GetPlayerName(targetid, pName, sizeof(pName));
				GetPlayerName(targetid, pNameGive, sizeof(pNameGive));

				if(adminlevel >= 0 && adminlevel <= 6) {} else return 1;

				if (PlayerInfo[playerid][pAdmin] == 6 || IsPlayerAdmin(playerid))
				{
				    if(IsPlayerConnected(targetid))
				    {
				        if(targetid != INVALID_PLAYER_ID)
				        {
				            GetPlayerName(targetid, pNameGive, sizeof(pNameGive));

							PlayerInfo[targetid][pAdmin] = adminlevel;
							Update(targetid, pAdminu);

							printf("AdmCmd: %s has promoted %s to a level %d admin.", pName, pNameGive, adminlevel);

							format(string, sizeof(string), "You have been promoted to a level %d admin by %s", adminlevel, pName);
							SendClientMessage(targetid, COLOR_LIGHTRED2, string);

							format(string, sizeof(string), "You have promoted %s to a level %d admin.", pNameGive, adminlevel);
							SendClientMessage(playerid, COLOR_LIGHTRED2, string);
						}
					}
					else
					{
						SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername.");
					}
				}

				else
				{
					SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
				}
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
		return 1;
	}

	if(strcmp(cmd, "/makehelper", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(gLogged[playerid] == 1)
		    {
				temp = strtok(cmdtext, idx);

				if(!strlen(temp))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: /makehelper <playerid/PartOfName> <helper level (1-6) >");
					return 1;
				}
				new helperlevel;

				targetid = ReturnUser(temp);
				temp = strtok(cmdtext, idx);
				helperlevel = strval(temp);

				if(helperlevel >= 0 && helperlevel <= 6) {} else return 1;

				if (PlayerInfo[playerid][pAdmin] == 6 || IsPlayerAdmin(playerid))
				{
				    if(IsPlayerConnected(targetid))
				    {
				        if(targetid != INVALID_PLAYER_ID)
				        {
				            GetPlayerName(targetid, pNameGive, sizeof(pNameGive));

							PlayerInfo[targetid][pHelper] = helperlevel;
							Update(targetid, pHelperu);

							printf("AdmCmd: %s has promoted %s to a level %d admin.", pName, pNameGive, helperlevel);

							format(string, sizeof(string), "You have been promoted to a level %d helper by %s", helperlevel, pName);
							SendClientMessage(targetid, COLOR_LIGHTRED2, string);

							format(string, sizeof(string), "You have promoted %s to a level %d helper.", pNameGive, helperlevel);
							SendClientMessage(playerid, COLOR_LIGHTRED2, string);
						}
					}
					else
					{
						SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername.");
					}
				}
				else
				{
					SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
				}
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
		return 1;
	}

	if(strcmp(cmd, "/makeleader", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(gLogged[playerid] == 1)
		    {
				temp = strtok(cmdtext, idx);

				if(!strlen(temp))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: /makeleader <playerid/PartOfName> <Faction Number (1-6) >");
					return 1;
				}

				new factionid;

				targetid = ReturnUser(temp);
				temp = strtok(cmdtext, idx);
				factionid = strval(temp);

				if(factionid < 1 || factionid > 6)
				{
					SendClientMessage(playerid, COLOR_ERROR, "Faction ID can't be lower than 1 or bigger than 6!");
					return 1;

				}

				if (PlayerInfo[playerid][pAdmin] == 6)
				{
				    if(IsPlayerConnected(targetid))
				    {
						if(targetid != INVALID_PLAYER_ID)
						{
						    GetPlayerName(targetid, pNameGive, sizeof(pNameGive));

							PlayerInfo[targetid][pLeader] = factionid;
							PlayerInfo[targetid][pMember] = factionid;
							
	                        Update(targetid, pLeaderu);
	                        Update(targetid, pMemberu);

							format(string, sizeof(string), "You got promoted to the rank of a faction leader by %s", pName);
							SendClientMessage(targetid, COLOR_LIGHTRED2, string);

							format(string, sizeof(string), "%s is now the leader of faction id %d.", pNameGive, factionid);
							SendClientMessage(playerid, COLOR_LIGHTRED2, string);

							if(factionid == 1)
							{
								PlayerInfo[targetid][pSkin] = 265;
								PlayerInfo[targetid][pRank] = 6;

								PlayerInfo[targetid][pRank] = 6;
								Update(targetid, pRanku);

								SetPlayerSkin(targetid, PlayerInfo[targetid][pSkin]);
								Update(targetid, pSkinu);																																																																																																																																						                            return 1;
							}
							else if(factionid == 2)
							{
								PlayerInfo[targetid][pSkin] = 265;
								PlayerInfo[targetid][pRank] = 6;

								PlayerInfo[targetid][pRank] = 6;
								Update(targetid, pRanku);

								SetPlayerSkin(targetid, PlayerInfo[targetid][pSkin]);
								Update(targetid, pSkinu);
							}
							else if(factionid == 3)
							{
								PlayerInfo[targetid][pSkin] = 265;
								PlayerInfo[targetid][pRank] = 6;

								PlayerInfo[targetid][pRank] = 6;
								Update(targetid, pRanku);

								SetPlayerSkin(targetid, PlayerInfo[targetid][pSkin]);
								Update(targetid, pSkinu);
							}
							else if(factionid == 4)
							{
								PlayerInfo[targetid][pSkin] = 295;
								PlayerInfo[targetid][pRank] = 6;

								PlayerInfo[targetid][pRank] = 6;
								Update(targetid, pRanku);

								SetPlayerSkin(targetid, PlayerInfo[targetid][pSkin]);
								Update(targetid, pSkinu);
							}
							else if(factionid == 5)
							{
								PlayerInfo[targetid][pSkin] = 287;
							 	PlayerInfo[targetid][pRank] = 6;

								PlayerInfo[targetid][pRank] = 6;
								Update(targetid, pRanku);

								SetPlayerSkin(targetid, PlayerInfo[targetid][pSkin]);
								Update(targetid, pSkinu);
							}
							else if(factionid == 6)
							{
								PlayerInfo[targetid][pSkin] = 228;
							 	PlayerInfo[targetid][pRank] = 6;

								PlayerInfo[targetid][pRank] = 6;
								Update(targetid, pRanku);

								SetPlayerSkin(targetid, PlayerInfo[targetid][pSkin]);
								Update(targetid, pSkinu);
							}
						}
					}
					else
					{
						SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername.");
					}
				}
				else
				{
					SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
				}
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
		return 1;
	}

	if(strcmp(cmd, "/auninvite", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(gLogged[playerid] == 1)
		    {
				temp = strtok(cmdtext, idx);
				targetid = ReturnUser(temp);

				if(!strlen(temp))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: /auninvite <playerid/PartOfName>");
					return 1;
				}

				if (PlayerInfo[playerid][pAdmin] == 6)
				{
	   				if(IsPlayerConnected(targetid))
			 		{
					        if(targetid != INVALID_PLAYER_ID)
					        {
					            GetPlayerName(targetid, pNameGive, sizeof(pNameGive));

					        	format(string, sizeof(string), "* You were FactionPKed by Admin %s, you are kicked out of your faction.", pName);
					        	SendClientMessage(targetid, COLOR_LIGHTRED, string);

					        	SendClientMessage(targetid, COLOR_LIGHTBLUE, "* You're a civilian now.");

	                            PlayerInfo[targetid][pLeader] = 0;
					        	Update(targetid, pLeaderu);

					        	PlayerInfo[targetid][pMember] = 0;
					        	Update(targetid, pMemberu);

					        	PlayerInfo[targetid][pRank] = 0;
					        	Update(targetid, pRanku);

	                            PlayerInfo[targetid][pSkin] = 250;
								SetPlayerSkin(targetid, PlayerInfo[targetid][pSkin]);
					        	Update(targetid, pSkinu);

	                            SetPlayerFactionColor(playerid);

								SetSpawnInfo(playerid, 0, 0, 1742.9515, -1862.3721, 13.5764, 359.5591, 0, 0, 0, 0, 0, 0);
	                            SetPlayerInterior(playerid, 0);
								SpawnPlayer(targetid);
							}
						}
						else
						{
							SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername.");
						}
					}
					else
					{
						SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
					}
				}
				else
				{
			    	SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
				}
		}
		return 1;
	}

	if(strcmp(cmd, "/respawn", true) ==0)
	{
		if(IsPlayerConnected(playerid))
		{
		    if(gLogged[playerid] == 1)
		    {
			    temp = strtok(cmdtext, idx);

			    if(!strlen(temp))
			    {
			        SendClientMessage(playerid, COLOR_SYN, "Synthax: /respawn <playerid/PartOfName>");
			        return 1;
	      		}

	      		targetid = ReturnUser(temp);

	      		if(PlayerInfo[playerid][pAdmin] >= 1)
	      		{
					if(IsPlayerConnected(targetid))
					{
					    GetPlayerName(playerid, pName, sizeof(pName));
					    GetPlayerName(targetid, pNameGive, sizeof(pNameGive));

					    if(targetid != INVALID_PLAYER_ID)
					    {
					        format(string, sizeof(string), "AdmWarning: %s respawned %s.", pName, pNameGive);
					        AdminsBroadCast(COLOR_LIGHTRED, string);

					        format(string, sizeof(string), "AdmCmd: %s respawned you.", pName);
					    	SendClientMessage(targetid, COLOR_LIGHTRED, string);

					        SetPlayerInterior(playerid, 0);
					        SpawnPlayer(targetid);
						}
					}
					else
					{
						SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername.");
					}
				}
				else
				{
					SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
				}
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
		return 1;
	}

    if(strcmp(cmd, "/checkweapons", true) == 0 || strcmp(cmd, "/checkw", true) == 0)
	{
		if(IsPlayerConnected(playerid))
		{
			if(gLogged[playerid] == 1)
			{
				if (PlayerInfo[playerid][pAdmin] < 1)
			    {
				    SendClientMessage(playerid, COLOR_ERROR, "You are not authorized");
				    return 1;
			    }
				temp = strtok(cmdtext,idx);
				if (!strlen(temp))
			    {
				    SendClientMessage(playerid, COLOR_SYN, "Synthax: /checkweapons <playerid/partOfName>");
				    return 1;
			    }
				targetid = ReturnUser(temp);
				if (targetid == INVALID_PLAYER_ID)
			    {
				    SendClientMessage(playerid, COLOR_ERROR, "That player is offline");
				    return 1;
			    }
				new weaponsmodel, ammunition;

			    GetPlayerName(targetid, pNameGive, sizeof(pNameGive));

			    format(string, sizeof(string), "%s has the following weapons:", pNameGive);
			    SendClientMessage(playerid, COLOR_ADMIN_INFO, string);

				for (new i=0; i<9; i++)
			    {
				    GetPlayerWeaponData(targetid, i, weaponsmodel, ammunition);
				    if(weaponsmodel != 0)
				    {
				        format(string, sizeof(string), "%d: %d (%d)", i, weaponsmodel, ammunition);
				    	SendClientMessage(playerid, COLOR_ADMIN_INFO, string);
					}
			    }
			}
		}
		return 1;
	}

    if(strcmp(cmd, "/spec", true) == 0 || strcmp(cmd, "/recon", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(PlayerInfo[playerid][pAdmin] < 1)
			{
				SendClientMessage(playerid, COLOR_ERROR, "You are not authorized to use that command.");
				return 1;
			}
			temp = strtok(cmdtext, idx);
			if(!strlen(temp))
			{
				SendClientMessage(playerid, COLOR_SYN, "Synthax: /spec [playerid/off]");
				return 1;
			}
			targetid = ReturnUser(temp);
			if(IsPlayerNPC(targetid)) return 1;
			if(strcmp("off", temp, true, strlen(temp)) == 0)
			{
			    if(GetPlayerState(playerid) != PLAYER_STATE_SPECTATING)
			    {
			        SendClientMessage(playerid, COLOR_ADMIN_INFO, "You are not spectating anyone");
					return 1;
			    }
				SetPlayerInterior(playerid, PlayerInfo[playerid][pInterior]);
				SendClientMessage(playerid, COLOR_ADMIN_INFO, "You are no longer spectating.");
			    TogglePlayerSpectating(playerid, 0);
			    SpectatedID[playerid] = INVALID_PLAYER_ID;
			    SpectateType[playerid] = ADMIN_SPEC_TYPE_NONE;
			    return 1;
			}
			if(IsPlayerConnected(targetid))
			{
				if(GetPlayerState(playerid) != PLAYER_STATE_SPECTATING)
				{
				    PlayerInfo[playerid][pInterior] = GetPlayerInterior(playerid);
				}
				SetPlayerVirtualWorld(playerid, GetPlayerVirtualWorld(targetid));
				SetPlayerInterior(playerid, GetPlayerInterior(targetid));
				TogglePlayerSpectating(playerid, 1);
				SpectatedID[playerid] = targetid;
				new Float:X, Float:Y, Float:Z;
				new Float:health;
				GetPlayerHealth(targetid, health);
				new Float:armor;
				GetPlayerArmour(targetid, armor);GetPlayerPos(playerid, X, Y, Z);
				GetPlayerName(targetid, pNameGive, sizeof(pNameGive));
				new cash =  GetPlayerMoney(targetid);
				if (PlayerInfo[playerid][pAdmin] >= 1)
				{
					format(string, sizeof(string), "Specing: [%d]%s [$%d] [H:%.0f] [A:%.0f]",targetid,pNameGive,cash,health,armor);
				}
				SendClientMessage(playerid, COLOR_GREEN, string);
				if(IsPlayerInAnyVehicle(targetid))
				{
				    PlayerSpectateVehicle(playerid, GetPlayerVehicleID(targetid));
				    SpectateType[playerid] = ADMIN_SPEC_TYPE_VEHICLE;
				}
				else
				{
				    PlayerSpectatePlayer(playerid, targetid);
				    SpectateType[playerid] = ADMIN_SPEC_TYPE_PLAYER;
				}
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "That player isn't active !");
			    return 1;
			}
	    }
	    return 1;
	}

    if(strcmp(cmd, "/gethere", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(gLogged[playerid] == 1)
		    {
				temp = strtok(cmdtext, idx);

				if(!strlen(temp))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: /gethere <playerid/PartOfName> or <carid>");
					return 1;
				}
				targetid = ReturnUser(temp);
				if (PlayerInfo[playerid][pAdmin] >= 1)
				{
					if (IsPlayerConnected(targetid))
					{
					    if(targetid != INVALID_PLAYER_ID)
					    {
							GetPlayerPos(playerid, x, y, z);

							if(PlayerInfo[playerid][pInterior] > 0)
							{
								SetPlayerInterior(targetid,PlayerInfo[playerid][pInterior]);
								PlayerInfo[targetid][pInterior] = PlayerInfo[playerid][pInterior];
								SetPlayerInterior(targetid, GetPlayerInterior(playerid));
								SetPlayerVirtualWorld(targetid, GetPlayerVirtualWorld(playerid));
							}
							if(PlayerInfo[playerid][pInterior] == 0)
							{
								SetPlayerInterior(targetid,0);
							}
							if(z > 930.0 && PlayerInfo[playerid][pInterior] == 0)
							{
								SetPlayerInterior(targetid,1);
								PlayerInfo[targetid][pInterior] = 1;
							}
							if (GetPlayerState(targetid) == 2)
							{
								new tmpcar = GetPlayerVehicleID(targetid);
								SetVehiclePos(tmpcar, x, y+4, z);
								SetPlayerInterior(targetid, GetPlayerInterior(playerid));
								SetPlayerVirtualWorld(targetid, GetPlayerVirtualWorld(playerid));
								SetVehicleVirtualWorld(GetPlayerVehicleID(targetid), GetPlayerVirtualWorld(playerid));
							}
							else
							{
								SetPlayerPos(targetid,x, y+2, z);
							}
							SendClientMessage(targetid, COLOR_LIGHTRED, "You got teleported");
						}
					}
					else if(strcmp(temp, "car", true) == 0)
						{
		                    temp = strtok(cmdtext, idx);

							if(!strlen(temp))
							{
								SendClientMessage(playerid, COLOR_SYN, "Synthax: /gethere car <carid>");
								return 1;
							}

							new carid;

							carid = strval(temp);

							GetPlayerPos(playerid, x, y, z);
							SetVehiclePos(carid,x,y+4, z);
							SetVehicleVirtualWorld(carid, GetPlayerVirtualWorld(playerid));
						}
				}
				else
				{
					SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
				}
			}
			else if(gLogged[playerid] != 1)
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
			else
			{
				SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername / carid.");
			}
		}
		return 1;
	}

    //
	// Helpers Commands
	if(strcmp(cmd, "/hh", true) == 0 || strcmp(cmd, "/helperhelp", true) == 0)
	{
		if(IsPlayerConnected(playerid))
		{
		    if(gLogged[playerid] == 1)
		    {
				if(PlayerInfo[playerid][pHelper] >= 1)
				{
				    SendClientMessage(playerid, COLOR_GREEN, "_______________________________________");

				    if(PlayerInfo[playerid][pHelper] >= 1)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Helper level 1 ** (/hh)elperhelp (/he)lperchat /goto /check");
				    }

				    if(PlayerInfo[playerid][pHelper] >= 2)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Helper level 2 **");
				    }

				    if(PlayerInfo[playerid][pHelper] >= 3)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Helper level 3 **");
				    }

				    if(PlayerInfo[playerid][pHelper] >= 4)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Helper level 4 **");
				    }

				    if(PlayerInfo[playerid][pHelper] >= 5)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Helper level 5 **");
				    }

				    if(PlayerInfo[playerid][pHelper] >= 6)
				    {
				    	SendClientMessage(playerid, COLOR_WHITE, "** Helper level 6 **");
				    }

				    SendClientMessage(playerid, COLOR_GREEN, "_______________________________________");
				}

			    else
	  			{
	  				SendClientMessage(playerid, COLOR_ERROR, "Error: Your helper level is not high enough to use this command.");
				}

	  		}

			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
	  		}
		}
		return 1;
	}

	if(strcmp(cmd, "/he", true) == 0 || strcmp(cmd, "/helperchat", true) == 0)
    {
        if(IsPlayerConnected(playerid))
		{
		    if(gLogged[playerid] == 1)
		    {
			    GetPlayerName(playerid, pName, sizeof(pName));

				new length = strlen(cmdtext);

				while ((idx < length) && (cmdtext[idx] <= ' '))
				{
					idx++;
				}

				new offset = idx;
				new result[128];

				while ((idx < length) && ((idx - offset) < (sizeof(result) - 1)))
				{
					result[idx - offset] = cmdtext[idx];
					idx++;
				}

				result[idx - offset] = EOS;

				if(!strlen(result))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: (/he)lperchat <text>");
					return 1;
				}

		        if (PlayerInfo[playerid][pHelper] >= 1 && PlayerInfo[playerid][pHelper] <= 5)
		        {
		        	format(string, sizeof(string), "** Helper Level %d %s : %s **", PlayerInfo[playerid][pHelper], pName, result);
		        	HelpersBroadCast(COLOR_YELLOW2, string);
		        }

		        if (PlayerInfo[playerid][pHelper] == 6)
		        {
		        	format(string, sizeof(string), "{CFAE70}** Helper Level %d %s : %s **", PlayerInfo[playerid][pHelper], pName, result);
		        	HelpersBroadCast(COLOR_WHITE, string);
		        }

		        if (PlayerInfo[playerid][pAdmin] < 1)
				{
	       			SendClientMessage(playerid, COLOR_ERROR, "Error: Your helper level is not high enough to use this command.");
			    	return 1;
	  		    }
	        }
	        else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
        }
    	return 1;
    }

    //
    // Admins & Helpers commands
    if(strcmp(cmd, "/goto", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(gLogged[playerid] == 1)
		    {
				temp = strtok(cmdtext, idx);

				if(!strlen(temp))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: /goto <playerid/PartOfName> or <carid> or <destination>");
					return 1;
				}

				targetid = ReturnUser(temp);

				if (PlayerInfo[playerid][pAdmin] >= 1 || PlayerInfo[playerid][pHelper] >= 1)
				{
					if (IsPlayerConnected(targetid))
					{
					    if(targetid != INVALID_PLAYER_ID)
					    {
							if(GetPlayerState(targetid) == PLAYER_STATE_SPECTATING)
							{
								SendClientMessage(playerid, COLOR_YELLOW3, "That Player is spectating Someone.");
							}

							GetPlayerPos(targetid, Float:x, Float:y, Float:z);

							if(PlayerInfo[targetid][pInterior] > 0)
							{
								SetPlayerInterior(playerid,PlayerInfo[targetid][pInterior]);
								PlayerInfo[playerid][pInterior] = PlayerInfo[targetid][pInterior];
							}
							if(PlayerInfo[playerid][pInterior] == 0)
							{
								SetPlayerInterior(playerid,0);
							}
							if(Float:z > 530.0 && PlayerInfo[targetid][pInterior] == 0)
							{
								SetPlayerInterior(playerid,1);
								PlayerInfo[playerid][pInterior] = 1;
							}
							if (GetPlayerState(playerid) == 2)
							{
								new tempcar = GetPlayerVehicleID(playerid);
								SetVehiclePos(tempcar, Float:x, Float:y+4, Float:z);
							}
							else
							{
								SetPlayerPos(playerid, Float:x, Float:y+2, Float:z);
							}

							SendClientMessage(playerid, COLOR_LIGHTRED, "You got teleported");
							SetPlayerInterior(playerid, GetPlayerInterior(targetid));
							SetPlayerVirtualWorld(playerid, GetPlayerVirtualWorld(targetid));
						}
					}
					else if(strcmp(temp, "car", true) == 0)
					{
	                    temp = strtok(cmdtext, idx);
						if(!strlen(temp))
						{
						    SendClientMessage(playerid, COLOR_SYN, "Synthax: /goto car <carid>");
							return 1;
						}

				    	new tempcar = strval(temp);

						GetVehiclePos(tempcar, Float:x, Float:y, Float:z);

						if (GetPlayerState(playerid) == 2)
						{
							new tempcar2 = GetPlayerVehicleID(playerid);
							SetVehiclePos(tempcar2, Float:x, Float:y, Float:z);
						}
						else
						{
							SetPlayerPos(playerid, Float:x, Float:y, Float:z);
						}

						SendClientMessage(playerid, COLOR_LIGHTRED, "You got teleported");
						SetPlayerInterior(playerid, 0);
					}
					else if(strcmp(temp, "ls", true) == 0)
					{
				    	if (GetPlayerState(playerid) == 2)
						{
							new tempcar = GetPlayerVehicleID(playerid);
							SetVehiclePos(tempcar, 1529.6,-1691.2,13.3);
						}
						else
						{
							SetPlayerPos(playerid, 1529.6,-1691.2,13.3);
						}

						SendClientMessage(playerid, COLOR_LIGHTRED, "You got teleported");
						SetPlayerInterior(playerid,0);
						PlayerInfo[playerid][pInterior] = 0;
					}
					else
					{
		   				SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername / carid / destination.");
					}
				}
				else
				{
				SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
				}
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
		return 1;
	}

    if(strcmp(cmd, "/check", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(gLogged[playerid] == 1)
		    {
				temp = strtok(cmdtext, idx);

				if(!strlen(temp))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: /check <playerid/PartOfName>");
					return 1;
				}

	            if (PlayerInfo[playerid][pAdmin] >= 1 || PlayerInfo[playerid][pHelper] >= 1)
				{

					targetid = ReturnUser(temp);

					if(targetid != INVALID_PLAYER_ID)
					{
					    GetPlayerName(targetid, pNameGive, sizeof(pNameGive));

						ShowStats(playerid, targetid);
					}

					else
					{
					    SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername.");
	    			}
				}

				else
				{
					SendClientMessage(playerid, COLOR_ERROR, "Error: Your admin level is not high enough to use this command.");
				}
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
		return 1;
	}
	//
	// Leader commands
	if(strcmp(cmd, "/uninvite", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
	        if(gLogged[playerid] == 1)
		    {
				temp = strtok(cmdtext, idx);
				if(!strlen(temp))
				{
					SendClientMessage(playerid, COLOR_SYN, "Synthax: /uninvite [1 with Fanction Punish/0 without Fanction Punish] <playerid/PartOfName>");
					return 1;
				}
				new x_nr;
				x_nr = strvalEx(temp);
				temp = strtok(cmdtext, idx);
				targetid = ReturnUser(temp);
	   			if(x_nr == 0)
				{
					if(!strlen(temp))
					{
						SendClientMessage(playerid, COLOR_SYN, "Synthax: /uninvite 0 <playerid/PartOfName>");
						return 1;
					}
					if (PlayerInfo[playerid][pLeader] >= 1 || PlayerInfo[playerid][pAdmin] >= 5 )
					{
					    if(IsPlayerConnected(targetid))
					    {
					        if(targetid != INVALID_PLAYER_ID)
					        {
								if(PlayerInfo[playerid][pLeader] != PlayerInfo[targetid][pMember])
								{
									SendClientMessage(playerid, COLOR_ERROR, "Uninvite: That player is not in your faction!");
									return 1;
								}

								format(string, sizeof(string), "* You got fired by %s.", pName);
								SendClientMessage(targetid, COLOR_LIGHTRED, string);

								SendClientMessage(targetid, COLOR_LIGHTBLUE, "* You're a civilian now.");

								PlayerInfo[targetid][pMember] = 0;
								Update(targetid, pMemberu);

								PlayerInfo[targetid][pRank] = 0;
								Update(targetid, pRanku);

								format(string, sizeof(string), "You fired %s.", pNameGive);
								SendClientMessage(playerid, COLOR_LIGHTRED, string);
							}
						}
						else
						{
							SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername.");
						}
					}
					else
					{
						SendClientMessage(playerid, COLOR_ERROR, "** You are not autorized to use this command!");
					}
				}
				if(x_nr == 1)
				{
					if(!strlen(temp))
					{
						SendClientMessage(playerid, COLOR_SYN, "Synthax: /uninvite 1 <playerid/PartOfName>");
						return 1;
					}
					if (PlayerInfo[playerid][pLeader] >= 1 || PlayerInfo[playerid][pAdmin] >= 6 )
					{
					    if(IsPlayerConnected(targetid))
					    {
					        if(targetid != INVALID_PLAYER_ID)
					        {
							    if (PlayerInfo[targetid][pMember] > 0)
							    {
							        if(PlayerInfo[playerid][pLeader] != PlayerInfo[targetid][pMember])
									{
										SendClientMessage(playerid, COLOR_ERROR, "Uninvite: That player is not in your faction!");
										return 1;
									}

									format(string, sizeof(string), "* You got fired by %s.", pName);
									SendClientMessage(targetid, COLOR_LIGHTBLUE, string);

									SendClientMessage(targetid, COLOR_LIGHTBLUE, "* You're a civilian now.");

									PlayerInfo[targetid][pMember] = 0;
									Update(targetid, pMemberu);

									PlayerInfo[targetid][pRank] = 0;
									Update(targetid, pRanku);

									SpawnPlayer(targetid);
									format(string, sizeof(string), " You dismissed %s.", pNameGive);
									SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
								}
							}
						}
						else
						{
							SendClientMessage(playerid, COLOR_ERROR, "Error: Invalid playerid / playername.");
						}
					}
					else
					{
						SendClientMessage(playerid, COLOR_ERROR, "** You are not autorized to use this command!!");
					}
				}
			}
			else
			{
			    SendClientMessage(playerid, COLOR_ERROR, "You are not logged in!");
			}
		}
		return 1;
	}
	//
	// Faction Member Commands
	if(strcmp(cmd, "/f", true) == 0 || strcmp(cmd, "/faction", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
			GetPlayerName(playerid, pName, sizeof(pName));
			new length = strlen(cmdtext);

			while ((idx < length) && (cmdtext[idx] <= ' '))
			{
				idx++;
			}

			new offset = idx;
			new result[128];

			while ((idx < length) && ((idx - offset) < (sizeof(result) - 1)))
			{
				result[idx - offset] = cmdtext[idx];
				idx++;
			}

			result[idx - offset] = EOS;

			if(!strlen(result))
			{
				SendClientMessage(playerid, COLOR_SYN, "Synthax: (/f)action <faction chat>");
				return 1;
			}

			if(PlayerInfo[playerid][pLeader] > 0 || PlayerInfo[playerid][pMember] > 0)
			{
			    if(PlayerInfo[playerid][pMember] == 1)
			    {
		 			if(PlayerInfo[playerid][pRank] == 6) { format(string, sizeof(string), "** (( Chief %s: %s )) **", pName, result); }
					else if(PlayerInfo[playerid][pRank] == 5) { format(string, sizeof(string), "** (( Captain %s: %s ))  **", pName, result); }
					else if(PlayerInfo[playerid][pRank] == 4) { format(string, sizeof(string), "** (( Lieutenant %s: %s )) **", pName, result); }
					else if(PlayerInfo[playerid][pRank] == 3) { format(string, sizeof(string), "** (( Sergeant %s: %s ))  **", pName, result); }
					else if(PlayerInfo[playerid][pRank] == 2) { format(string, sizeof(string), "** (( Officer %s: %s ))  **", pName, result); }
					else if(PlayerInfo[playerid][pRank] == 1) { format(string, sizeof(string), "** (( Cadet %s: %s ))  **", pName, result); }
					else { format(string, sizeof(string), "** (( Cadet %s: %s )) **", pName, result); }
					FactionsBroadCast(PlayerInfo[playerid][pMember], COLOR_FACTION_CHAT, string);
			    }
			    else if(PlayerInfo[playerid][pMember] == 4)
			    {
				    if(PlayerInfo[playerid][pRank] == 6) { format(string, sizeof(string), "** (( Chief Fireman/Paramedic %s: %s )) **", pName, result); }
					else if(PlayerInfo[playerid][pRank] == 5) { format(string, sizeof(string), "** (( Paramedic Field %s: %s )) **", pName, result); }
					else if(PlayerInfo[playerid][pRank] == 4) { format(string, sizeof(string), "** (( Ambulance Commander %s: %s ))  **", pName, result); }
					else if(PlayerInfo[playerid][pRank] == 3) { format(string, sizeof(string), "** (( Paramedic in charge %s: %s ))  **", pName, result); }
					else if(PlayerInfo[playerid][pRank] == 2) { format(string, sizeof(string), "** (( Fire Paramedic %s: %s ))  **", pName, result); }
					else if(PlayerInfo[playerid][pRank] == 1) { format(string, sizeof(string), "** (( Candidate Fire Paramedic %s: %s ))  **", pName, result); }
					else { format(string, sizeof(string), "** (( Private %s: %s )) **", pName, result); }
					FactionsBroadCast(PlayerInfo[playerid][pMember], COLOR_FACTION_CHAT, string);
			    }
				return 1;
			}
			else
			{
				SendClientMessage(playerid, COLOR_ERROR, "Error: You are not part of a Faction!");
				return 1;
			}
		}
		return 1;
	}

	if(strcmp(cmd, "/radio", true) == 0 || strcmp(cmd, "/r", true) == 0)
	{
	    if(IsPlayerConnected(playerid))
	    {
			GetPlayerName(playerid, pName, sizeof(pName));

			new length = strlen(cmdtext);

			while ((idx < length) && (cmdtext[idx] <= ' '))
			{
				idx++;
			}

			new offset = idx;
			new result[128];

			while ((idx < length) && ((idx - offset) < (sizeof(result) - 1)))
			{
				result[idx - offset] = cmdtext[idx];
				idx++;
			}

			result[idx - offset] = EOS;

			if(!strlen(result))
			{
				SendClientMessage(playerid, COLOR_SYN, "Synthax: (/r)adio <radio chat>");
				return 1;
			}

			if(PlayerInfo[playerid][pMember] == 1 || PlayerInfo[playerid][pLeader] == 1)
			{
				if(PlayerInfo[playerid][pRank] >= 6) { format(string, sizeof(string), "** LSPD Chief %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 5) { format(string, sizeof(string), "** LSPD Captain %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 4) { format(string, sizeof(string), "** LSPD Lieutenant %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 3) { format(string, sizeof(string), "** LSPD Sergeant %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 2) { format(string, sizeof(string), "** LSPD Officer %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 1) { format(string, sizeof(string), "** LSPD Cadet %s: %s, over. **", pName, result); }
				else
				{
					format(string, sizeof(string), "** LSPD Cadet %s: %s, over. **", pName, result);
				}
				RadioBroadCast(PlayerInfo[playerid][pMember], COLOR_RADIO_CHAT, string);
				return 1;
			}
			if(PlayerInfo[playerid][pMember] == 2 || PlayerInfo[playerid][pLeader] == 2)
			{
				if(PlayerInfo[playerid][pRank] >= 6) { format(string, sizeof(string), "** LVPD Chief %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 5) { format(string, sizeof(string), "** LVPD Captain %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 4) { format(string, sizeof(string), "** LVPD Lieutenant %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 3) { format(string, sizeof(string), "** LVPD Sergeant %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 2) { format(string, sizeof(string), "** LVPD Officer %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 1) { format(string, sizeof(string), "** LVPD Cadet %s: %s, over. **", pName, result); }
				else
				{
					format(string, sizeof(string), "** Cadet %s: %s, over. **", pName, result);
				}
				RadioBroadCast(PlayerInfo[playerid][pMember], COLOR_RADIO_CHAT, string);
				return 1;
			}
			if(PlayerInfo[playerid][pMember] == 3 || PlayerInfo[playerid][pLeader] == 3)
			{
				if(PlayerInfo[playerid][pRank] >= 6) { format(string, sizeof(string), "** SFPD Chief %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 5) { format(string, sizeof(string), "** SFPD Captain %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 4) { format(string, sizeof(string), "** SFPD Lieutenant %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 3) { format(string, sizeof(string), "** SFPD Sergeant %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 2) { format(string, sizeof(string), "** SFPD Officer %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 1) { format(string, sizeof(string), "** SFPD Cadet %s: %s, over. **", pName, result); }
				else
				{
					format(string, sizeof(string), "** Cadet %s: %s, over. **", pName, result);
				}
				RadioBroadCast(PlayerInfo[playerid][pMember], COLOR_RADIO_CHAT, string);
				return 1;
			}
			else if(PlayerInfo[playerid][pMember] == 4 || PlayerInfo[playerid][pLeader] == 4)
			{
			    if(PlayerInfo[playerid][pRank] == 6) { format(string, sizeof(string), "** Director %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 5) { format(string, sizeof(string), "** Assistant Director in Charge %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 4) { format(string, sizeof(string), "** Special Agent in Charge %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 3) { format(string, sizeof(string), "** Special Agent %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 2) { format(string, sizeof(string), "** Special Agent Trainee %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 1) { format(string, sizeof(string), "** Professional Staff %s: %s, over. **", pName, result); }
				else
				{
					format(string, sizeof(string), "** Intern %s: %s, over. **", pName, result);
				}
				RadioBroadCast(PlayerInfo[playerid][pMember], COLOR_RADIO_CHAT, string);
				return 1;
			}
			else if(PlayerInfo[playerid][pMember] == 5 || PlayerInfo[playerid][pLeader] == 5)
			{
			    if(PlayerInfo[playerid][pRank] == 6) { format(string, sizeof(string), "** General %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 5) { format(string, sizeof(string), "** Lieutenant %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 4) { format(string, sizeof(string), "** Captain %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 3) { format(string, sizeof(string), "** Major %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 2) { format(string, sizeof(string), "** Sergeant %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 1) { format(string, sizeof(string), "** Private %s: %s, over. **", pName, result); }
				else
				{
					format(string, sizeof(string), "** Private %s: %s, over. **", pName, result);
				}
				RadioBroadCast(PlayerInfo[playerid][pMember], COLOR_RADIO_CHAT, string);
				return 1;
			}
			else if(PlayerInfo[playerid][pMember]== 6 || PlayerInfo[playerid][pLeader]== 6)
			{
				if(PlayerInfo[playerid][pRank] == 6) { format(string, sizeof(string), "** Chief Paramedic %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 5) { format(string, sizeof(string), "** Assistant Chief Paramedic %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 4) { format(string, sizeof(string), "** Ambulance Chief %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 3) { format(string, sizeof(string), "** Paramedic in Charge %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 2) { format(string, sizeof(string), "** Paramedic %s: %s, over. **", pName, result); }
				else if(PlayerInfo[playerid][pRank] == 1) { format(string, sizeof(string), "** Candidate Paramedic %s: %s, over. **", pName, result); }
				else
				{
					format(string, sizeof(string), "** Candidate Paramedic %s: %s, over. **", pName, result);
				}
				RadioBroadCast(PlayerInfo[playerid][pMember], COLOR_RADIO_CHAT, string);
				return 1;
			}
			else
			{
				SendClientMessage(playerid, COLOR_ERROR, "Error: You are not part of a Team!");
				return 1;
			}
		}
		return 1;
	}
	//
	return SendClientMessage(playerid, COLOR_YELLOW2, "Error: Unknown command");
}

public OnPlayerEnterVehicle(playerid, vehicleid, ispassenger)
{
	return 1;
}

public OnPlayerExitVehicle(playerid, vehicleid)
{
	return 1;
}

public OnPlayerStateChange(playerid, newstate, oldstate)
{
	return 1;
}

public OnPlayerEnterCheckpoint(playerid)
{
	return 1;
}

public OnPlayerLeaveCheckpoint(playerid)
{
	return 1;
}

public OnPlayerEnterRaceCheckpoint(playerid)
{
	return 1;
}

public OnPlayerLeaveRaceCheckpoint(playerid)
{
	return 1;
}

public OnRconCommand(cmd[])
{
	return 1;
}

public OnPlayerRequestSpawn(playerid)
{
	return 1;
}

public OnObjectMoved(objectid)
{
	return 1;
}

public OnPlayerObjectMoved(playerid, objectid)
{
	return 1;
}

public OnPlayerPickUpPickup(playerid, pickupid)
{
	return 1;
}

public OnVehicleMod(playerid, vehicleid, componentid)
{
	return 1;
}

public OnVehiclePaintjob(playerid, vehicleid, paintjobid)
{
	return 1;
}

public OnVehicleRespray(playerid, vehicleid, color1, color2)
{
	return 1;
}

public OnPlayerSelectedMenuRow(playerid, row)
{
	return 1;
}

public OnPlayerExitedMenu(playerid)
{
	return 1;
}

public OnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid)
{
	return 1;
}

public OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	return 1;
}

public OnRconLoginAttempt(ip[], password[], success)
{
	return 1;
}

public OnPlayerUpdate(playerid)
{
	return 1;
}

public OnPlayerStreamIn(playerid, forplayerid)
{
	return 1;
}

public OnPlayerStreamOut(playerid, forplayerid)
{
	return 1;
}

public OnVehicleStreamIn(vehicleid, forplayerid)
{
	return 1;
}

public OnVehicleStreamOut(vehicleid, forplayerid)
{
	return 1;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{

    if(dialogid == DIALOG_LOGIN)
	{
	    if(IsPlayerConnected(playerid))
	    {
			if(!response)
  			{
    			Kick(playerid);
                return 1;
            }
            if(strlen(inputtext) >= 3 && strlen(inputtext) <= 20)
     		{
     		    if(strfind(inputtext, "'", true) != -1)
     		    {
     		        new pName[64];
					GetPlayerName(playerid, pName, sizeof(pName));
					ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "Login", "Type your account's password below:", "Login", "Exit");
				}
     		    new tmppass[64];
      			mysql_real_escape_string(inputtext, tmppass);
      			if(strlen(tmppass) < 3)
     		    {
     		        new loginname[64];
					GetPlayerName(playerid,loginname,sizeof(loginname));
					ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD,"Login", "Type your account's password below:", "Login", "Exit");
				}
				OnPlayerLogin(playerid,tmppass);
    		}
      		else
      		{
				new loginname[64];
				GetPlayerName(playerid,loginname,sizeof(loginname));
				ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "Login", "Type your account's password below:", "Login", "Exit");
    		}
		}
	}

	if(dialogid == DIALOG_REGISTER)
	{
	    if(!response)
     	{
			Kick(playerid);
			return 1;
		}

		if(strlen(inputtext) >= 3 && strlen(inputtext) <= 20)
		{
		    new tmppass[64];
		    new pName[30];
		    new string[256];
			GetPlayerName(playerid, pName, sizeof(pName));

		    mysql_real_escape_string(inputtext, tmppass);

		    if(strlen(tmppass) < 3)
			{
       			format(string,sizeof(string),"{BECBFC}Welcome, {95A3FF}%s{BECBFC}!\n\nPlease enter your password below to login to your account.\n\n* {FFFFFF}Last login: %s *",pName);
				ShowPlayerDialog(playerid, DIALOG_LOGIN ,DIALOG_STYLE_PASSWORD,"Login", string, "Login", "Exit");
			}
			OnPlayerRegister(playerid,tmppass);
		}
		else
		{
		    ShowPlayerDialog(playerid, DIALOG_REGISTER, DIALOG_STYLE_PASSWORD, "Character Gender", "Enter your password below:", "Register", "Cancel");
  		}
	}

	if(dialogid == DIALOG_SEX)
	{
	    if(!response)
      	{
      	    ShowPlayerDialog(playerid, DIALOG_SEX, DIALOG_STYLE_MSGBOX, "Character Gender", "Are you a male or a female?", "Male", "Female");
  	    }
	    if(response)
	    {
	        PlayerInfo[playerid][pSex] = 1;
	        Update(playerid, pSexu);

	        RegistrationStep[playerid] = 1;

			ShowPlayerDialog(playerid, DIALOG_AGE, DIALOG_STYLE_INPUT, "Character Age", "OK, so you're a male.\n\nNow, how old are you?(7-50)", "Next", "Back");

			return 1;
     	}
     	else
     	{
     	    PlayerInfo[playerid][pSex] = 2;
	        Update(playerid, pSexu);

	        RegistrationStep[playerid] = 1;

	        ShowPlayerDialog(playerid, DIALOG_AGE, DIALOG_STYLE_INPUT, "Character Age", "OK, so you're a female.\n\nNow, how old are you?(7-50)", "Next", "Back");

	        return 1;
      	}
	}

	if(dialogid == DIALOG_AGE)
	{
    	if(response)
    	{
  		 	new age = strval(inputtext);
  		 	new string[256];
  		 	if(age > 6 && age < 101)
  		 	{
				PlayerInfo[playerid][pAge] = age;
				Update(playerid, pAgeu);

				RegistrationStep[playerid] = 2;

				format(string, sizeof(string), "OK, so you have %d years.\n\nPlease enter below you email adress:", age);
				ShowPlayerDialog(playerid, DIALOG_EMAIL, DIALOG_STYLE_INPUT, "Email", string, "Next", "Back");
			}
			else return ShowPlayerDialog(playerid, DIALOG_AGE, DIALOG_STYLE_INPUT, "Character Age", "Now, what is your age?(7-100)", "Next", "Back");
		}
		if(!response)
      	{
      	    ShowPlayerDialog(playerid, DIALOG_SEX, DIALOG_STYLE_MSGBOX, "Character Gender", "Are you a male or a female?", "Male", "Female");
  	    }
	}

	if(dialogid == DIALOG_EMAIL)
	{
		if(response)
    	{
    		new string[256];
       		if(strlen(inputtext) < 4 || strlen(inputtext) > 151)
		   	{
		   		SendClientMessage(playerid, COLOR_ERROR, "The name of the email can't pass 150 letters and can't be shorter than 5 letters!");
			  	ShowPlayerDialog(playerid,702,DIALOG_STYLE_INPUT,"Email","Please enter below you email adress:","Next","");
			  	return 1;
			}
			else
       		{
				strmid(PlayerInfo[playerid][pEmail], inputtext, 0, strlen(inputtext), 999);
				Update(playerid, pEmailu);
				format(string, sizeof(string), "OK, so you're email is %s.", inputtext);
				ShowPlayerDialog(playerid, DIALOG_REGISTER_FINISH, DIALOG_STYLE_MSGBOX,"Email", string, "Next", "");
			}
		}
	 	if(!response)
      	{
      	    ShowPlayerDialog(playerid, DIALOG_AGE, DIALOG_STYLE_INPUT, "Character Age", "Now, what is your age?(7-100)", "Next", "Back");
  	    }
	}

	if(dialogid == DIALOG_REGISTER_FINISH)
	{
	    if(response)
    	{
    	    RegistrationStep[playerid] = 0;
			TutorialTime[playerid] = 1;
		}
	}
	return 1;
}

public OnPlayerClickPlayer(playerid, clickedplayerid, source)
{
	return 1;
}

public ShowStats(playerid, targetid)
{
	if(IsPlayerConnected(targetid))
	{
	    new pName[MAX_PLAYER_NAME];
		GetPlayerName(targetid, pName, sizeof(pName));
		new string[256];

	    new level = PlayerInfo[targetid][pLevel];
	    new sex[8];
	    if(PlayerInfo[targetid][pSex] == 1)
		{
			sex = "Male";
		}
		else if(PlayerInfo[targetid][pSex] == 2)
		{
			sex = "Female";
		}
	    new age = PlayerInfo[targetid][pAge];
	    new cash = GetPlayerCash(targetid);
	    new account = PlayerInfo[targetid][pAccount];
	    new phonenumber = PlayerInfo[targetid][pPhoneNumber];
	    new premiumaccount[4];
		if(PlayerInfo[targetid][pPremiumAccount] == 1)
		{
		    premiumaccount = "Yes";
	 	}
	 	else
	 	{
	 	    premiumaccount = "No";
	  	}
	  	new factiontext[128];
	  	if(PlayerInfo[targetid][pMember] == 1 || PlayerInfo[targetid][pLeader] == 1)
	  	{
	  	    factiontext = "Los Santos Police Department";
		}
		if(PlayerInfo[targetid][pMember] == 2 || PlayerInfo[targetid][pLeader] == 2)
		{
			factiontext = "Las Venturas Police Department";
		}
		if(PlayerInfo[targetid][pMember] == 3 || PlayerInfo[targetid][pLeader] == 3)
		{
			factiontext = "San Fierro Police Department";
		}
		else if(PlayerInfo[targetid][pMember] == 4 || PlayerInfo[targetid][pLeader] == 4)
		{
			factiontext = "Federal Bureau of Investigations";
		}
		else if(PlayerInfo[targetid][pMember] == 5 || PlayerInfo[targetid][pLeader] == 5)
		{
			factiontext = "National Guard";
		}
		new ranktext[128];
		if(PlayerInfo[targetid][pMember] == 1 || PlayerInfo[targetid][pLeader] == 1 || PlayerInfo[targetid][pMember] == 2 || PlayerInfo[targetid][pLeader] == 2 || PlayerInfo[targetid][pMember] == 3 || PlayerInfo[targetid][pLeader] == 3)// Los Santos Police Department Ranks || Las Venturas Police Department Ranks || San Fierro Police Department Ranks
		{
		    if(PlayerInfo[targetid][pRank] == 1)
			{
				ranktext = "Cadet";
			}
			else if(PlayerInfo[targetid][pRank] == 2)
			{
				ranktext = "Officer";
			}
			else if(PlayerInfo[targetid][pRank] == 3)
			{
				ranktext = "Sergent";
			}
			else if(PlayerInfo[targetid][pRank] == 4)
			{
				ranktext = "Lieutenant";
			}
			else if(PlayerInfo[targetid][pRank] == 5)
			{
				ranktext = "Captain";
			}
	        else if(PlayerInfo[targetid][pRank] == 6)
			{
				ranktext = "Chief";
			}
			else
			{
				ranktext = "Cadet";
			}
		}
		else if(PlayerInfo[targetid][pMember] == 4 || PlayerInfo[targetid][pLeader] == 4)// FBI Ranks
		{
		    if(PlayerInfo[targetid][pRank] == 1)
			{
				ranktext = "Professional Staff";
			}
			else if(PlayerInfo[targetid][pRank] == 2)
			{
				ranktext = "Special Agent Trainee";
			}
			else if(PlayerInfo[targetid][pRank] == 3)
			{
				ranktext = "Special Agent";
			}
			else if(PlayerInfo[targetid][pRank] == 4)
			{
				ranktext = "Special Agent in Charge";
			}
			else if(PlayerInfo[targetid][pRank] == 5)
			{
				ranktext = "Assistant Director in Charge";
			}
		    else if(PlayerInfo[targetid][pRank] == 6)
			{
				ranktext = "Director";
			}
			else
			{
				ranktext = "Professional Staff";
			}
		}
		else if(PlayerInfo[targetid][pMember] == 5 || PlayerInfo[targetid][pLeader] == 5)// National Guard Ranks
		{
		    if(PlayerInfo[targetid][pRank] == 1)
			{
				ranktext = "Private";
			}
			else if(PlayerInfo[targetid][pRank] == 2)
			{
				ranktext = "Mayor";
			}
			else if(PlayerInfo[targetid][pRank] == 3)
			{
				ranktext = "Sergent";
			}
			else if(PlayerInfo[targetid][pRank] == 4)
			{
				ranktext = "Lieutenant";
			}
   			else if(PlayerInfo[targetid][pRank] == 5)
   			{
				ranktext = "Captain";
	   		}
			else if(PlayerInfo[targetid][pRank] == 6)
			{
				ranktext = "General";
			}
			else
			{
				ranktext = "Private";
			}
		}

	    SendClientMessage(playerid, COLOR_SERVER_GREEN,"_______________________________________");
	    format(string, sizeof(string), "%s's Stats", pName);
		SendClientMessage(playerid, COLOR_WHITE, string);
		format(string, sizeof(string), "Level:[%d] Sex:[%s] Age:[%d] Cash:[$%s] Bank:[$%s] Phone:[%s]", level, sex, age, FormatNumber(cash), FormatNumber(account), PhoneFormat(phonenumber));
		SendClientMessage(playerid, COLOR_WHITE,string);
		format(string, sizeof(string), "Premium Account:[%s]", premiumaccount);
		SendClientMessage(playerid, COLOR_WHITE,string);
		format(string, sizeof(string), "Faction:[%s] Rank:[%s]", factiontext, ranktext);
		SendClientMessage(playerid, COLOR_WHITE,string);
	    SendClientMessage(playerid, COLOR_SERVER_GREEN,"_______________________________________");

	}

	return 1;
}

public KickPublic(playerid)
{
	Kick(playerid);
}

public ProxDetector(Float:radi, playerid, string[], col1, col2, col3, col4, col5)
{
	if(IsPlayerConnected(playerid))
	{
		new Float:posx, Float:posy, Float:posz;
		new Float:oldposx, Float:oldposy, Float:oldposz;
		new Float:tempposx, Float:tempposy, Float:tempposz;
		GetPlayerPos(playerid, oldposx, oldposy, oldposz);
		for(new i = 0; i < MAX_PLAYERS; i++)
		{
			if(IsPlayerConnected(i))
			{
				//if(!BigEar[i])
				//{
				if(GetPlayerVirtualWorld(i) == GetPlayerVirtualWorld(playerid))
				{
						GetPlayerPos(i, posx, posy, posz);
						tempposx = (oldposx -posx);
						tempposy = (oldposy -posy);
						tempposz = (oldposz -posz);
						if (((tempposx < radi/16) && (tempposx > -radi/16)) && ((tempposy < radi/16) && (tempposy > -radi/16)) && ((tempposz < radi/16) && (tempposz > -radi/16)))
						{
							SendClientMessage(i, col1, string);
						}
						else if (((tempposx < radi/8) && (tempposx > -radi/8)) && ((tempposy < radi/8) && (tempposy > -radi/8)) && ((tempposz < radi/8) && (tempposz > -radi/8)))
						{
							SendClientMessage(i, col2, string);
						}
						else if (((tempposx < radi/4) && (tempposx > -radi/4)) && ((tempposy < radi/4) && (tempposy > -radi/4)) && ((tempposz < radi/4) && (tempposz > -radi/4)))
						{
							SendClientMessage(i, col3, string);
						}
						else if (((tempposx < radi/2) && (tempposx > -radi/2)) && ((tempposy < radi/2) && (tempposy > -radi/2)) && ((tempposz < radi/2) && (tempposz > -radi/2)))
						{
							SendClientMessage(i, col4, string);
						}
						else if (((tempposx < radi) && (tempposx > -radi)) && ((tempposy < radi) && (tempposy > -radi)) && ((tempposz < radi) && (tempposz > -radi)))
						{
							SendClientMessage(i, col5, string);
						}
				}
				else
				{
					SendClientMessage(i, col1, string);
				}
			}
		}
	}
	return 0;
}

public ClockUpdate()
{
	new hour, minutes, seconds, day, month, year;
	new string[256];
	gettime(hour, minutes, seconds);
	getdate(year, month, day);
	format(string,256,"%02d:%02d", hour, minutes);
	TextDrawSetString(ClockTime, string);
	format(string,256,"%02d/%02d/%d", day, month, year);
	TextDrawSetString(ClockDate, string);
}

public MoneyTimer()
{
    new pName[MAX_PLAYER_NAME];
    for(new i=0; i<MAX_PLAYERS; i++)
    {
        if(IsPlayerConnected(i))
        {
            if(GetPlayerCash(i) != GetPlayerMoney(i))
            {
                ResetMoneyBar(i);
                UpdateMoneyBar(i, GetPlayerCash(i));
                new hack = GetPlayerMoney(i) - GetPlayerCash(i);
                GetPlayerName(i, pName, sizeof(pName));
                printf("%s has picked up/attempted to spawn $%d.", pName, hack);
            }
        }
    }
}

public OtherTimers()
{
	for(new i = 0; i < MAX_PLAYERS; i ++)
	{
	    if(TutorialTime[i]  >= 1)
		{
	        TutorialTime[i] += 1;
	        if(TutorialTime[i] == 3)
	 		{
	 		    SendClientMessage(i, COLOR_WHITE, "Edit Here");
	 		}
	 		else if(TutorialTime[i] == 5)
	 		{
	 		    SendClientMessage(i, COLOR_WHITE, "Edit Here");

	 		    TutorialTime[i] = 0; PlayerInfo[i][pTutorial] = 1;
				Update(i, pTutorialu);

	 		    PlayerInfo[i][pLevel] = 1;
 				Update(i, pLevelu);
	 		    PlayerInfo[i][pRegistred] = 1;
				Update(i, pRegistredu);
				PlayerInfo[i][pAdmin] = 0;
				Update(i, pAdminu);

	 		    new randomphonenumber = 1000000 + random(9999999);
				PlayerInfo[i][pPhoneNumber] = randomphonenumber;
				Update(i, pPhoneNumberu);

                PlayerInfo[i][pInterior] = 0;

	 		    SetSpawnInfo(i, 0, 0, 1742.9515, -1862.3721, 13.5764, 359.5591, 0, 0, 0, 0, 0, 0);
				SpawnPlayer(i);

				new pName[MAX_PLAYER_NAME];
				GetPlayerName(i, pName, sizeof(pName));

				new query[128];
				mysql_format(handle, query, sizeof(query), "SELECT * FROM `players` WHERE `username`='%e'", pName);
        		mysql_tquery(handle, query, "WhenPlayerLogin", "i", i);

	 		}
		}

	    if(ReportTime[i] > 0)
	    {
	        ReportTime[i]--;
	    }
 	}
}

public ReportTimer(playerid)
{
    ReportTime[playerid] = 0;
}

public AdminsBroadCast(color, string[])
{
	for(new i = 0; i < MAX_PLAYERS; i++)
	{
		if(IsPlayerConnected(i))
		{
			if(PlayerInfo[i][pAdmin] >= 1)
			{
				SendClientMessage(i, color, string);
			}
		}
	}
	return 1;
}

public HelpersBroadCast(color, string[])
{
	for(new i = 0; i < MAX_PLAYERS; i++)
	{
		if(IsPlayerConnected(i))
		{
			if(PlayerInfo[i][pHelper] >= 1)
			{
				SendClientMessage(i, color, string);
			}
		}
	}
	return 1;
}

public FactionsBroadCast(faction, color, string[])
{
	for(new i = 0; i < MAX_PLAYERS; i++)
	{
		if(IsPlayerConnected(i))
		{
		    if(PlayerInfo[i][pMember] == faction || PlayerInfo[i][pLeader] == faction)
		    {
				SendClientMessage(i, color, string);
			}
		}
	}
}

public RadioBroadCast(faction, color, string[])
{
	for(new i = 0; i < MAX_PLAYERS; i++)
	{
		if(IsPlayerConnected(i))
		{
		    if(PlayerInfo[i][pMember] == faction || PlayerInfo[i][pLeader] == faction)
		    {
				SendClientMessage(i, color, string);
			}
		}
	}
}

public SetPlayerFactionColor(playerid)
{
	if(IsPlayerConnected(playerid))
	{
	    SetPlayerColor(playerid, COLOR_WHITE);
		if(PlayerInfo[playerid][pMember] == 1 || PlayerInfo[playerid][pLeader] == 1)
        SetPlayerColor(playerid,COLOR_LSPD);

        if (PlayerInfo[playerid][pMember] == 2 || PlayerInfo[playerid][pLeader] == 2)
        SetPlayerColor(playerid,COLOR_LVPD);

        if (PlayerInfo[playerid][pMember] == 3 || PlayerInfo[playerid][pLeader] == 3)
        SetPlayerColor(playerid,COLOR_SFPD);

	    if (PlayerInfo[playerid][pMember] == 4 || PlayerInfo[playerid][pLeader] == 4)
        SetPlayerColor(playerid,COLOR_FBI);

        if (PlayerInfo[playerid][pMember] == 5 || PlayerInfo[playerid][pLeader] == 5)
        SetPlayerColor(playerid,COLOR_NG);
        
        if (PlayerInfo[playerid][pMember] == 6 || PlayerInfo[playerid][pLeader] == 6)
        SetPlayerColor(playerid,COLOR_PARAMEDICS);
	}
 }

stock KickWithMessage(playerid, color, message[])
{
    SendClientMessage(playerid, color, message);
    SetTimerEx("KickPublic", 1000, 0, "d", playerid);
}

stock GivePlayerCash(playerid, money)
{
    Cash[playerid] += money;
    ResetMoneyBar(playerid);
    UpdateMoneyBar(playerid, Cash[playerid]);
    PlayerInfo[playerid][pCash] = Cash[playerid];
	Update(playerid, pCashu);

    return Cash[playerid];
}

stock SetPlayerCash(playerid, money)
{
    Cash[playerid] = money;
    ResetMoneyBar(playerid);
    UpdateMoneyBar(playerid, Cash[playerid]);
    PlayerInfo[playerid][pCash] = Cash[playerid];
	Update(playerid, pCashu);

	return Cash[playerid];
}

stock ResetPlayerCash(playerid)
{
    Cash[playerid] = 0;
    ResetMoneyBar(playerid);
    UpdateMoneyBar(playerid, Cash[playerid]);
    PlayerInfo[playerid][pCash] = Cash[playerid];
	Update(playerid, pCashx);

	return Cash[playerid];
}

stock GetPlayerCash(playerid)
{
    return Cash[playerid];
}
