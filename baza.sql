-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Gazda: 127.0.0.1
-- Timp de generare: 15 Noi 2013 la 21:05
-- Versiune server: 5.6.11
-- Versiune PHP: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Bază de date: `w3op`
--
CREATE DATABASE IF NOT EXISTS `w3op` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `w3op`;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `players`
--

CREATE TABLE IF NOT EXISTS `players` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(128) NOT NULL,
  `Email` varchar(150) DEFAULT NULL,
  `Level` int(11) NOT NULL DEFAULT '1',
  `AdminLevel` int(11) NOT NULL DEFAULT '0',
  `HelperLevel` int(11) NOT NULL DEFAULT '0',
  `Cash` int(24) NOT NULL DEFAULT '0',
  `Account` int(24) NOT NULL DEFAULT '0',
  `Registred` int(11) DEFAULT '0',
  `Tutorial` int(11) NOT NULL DEFAULT '0',
  `Sex` varchar(10) NOT NULL DEFAULT '0',
  `Age` int(24) NOT NULL DEFAULT '0',
  `PhoneNumber` int(24) NOT NULL DEFAULT '0',
  `PremiumAccount` int(11) NOT NULL DEFAULT '0',
  `Banned` int(11) NOT NULL,
  `Warns` int(11) NOT NULL,
  `Leader` int(11) NOT NULL DEFAULT '0',
  `Member` int(11) NOT NULL DEFAULT '0',
  `Rank` int(11) NOT NULL DEFAULT '0',
  `Skin` int(11) NOT NULL DEFAULT '0',
  `IP` varchar(46) DEFAULT NULL,
  `LastLogin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  FULLTEXT KEY `password` (`password`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Salvarea datelor din tabel `players`
--

INSERT INTO `players` (`ID`, `username`, `password`, `Email`, `Level`, `AdminLevel`, `HelperLevel`, `Cash`, `Account`, `Registred`, `Tutorial`, `Sex`, `Age`, `PhoneNumber`, `PremiumAccount`, `Banned`, `Warns`, `Leader`, `Member`, `Rank`, `Skin`, `IP`, `LastLogin`) VALUES
(15, '[S4L]WopsS', '67353f171fc3cc04003d0f947472b72a', NULL, 10, 0, 0, 0, 0, 0, 0, '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, '0.0.0.0', '2013-11-14 05:30:42');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
