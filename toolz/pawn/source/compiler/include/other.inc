ReturnUser(text[], playerid = INVALID_PLAYER_ID)
{
	new pos = 0;
	while (text[pos] < 0x21) // Strip out leading spaces
	{
		if (text[pos] == 0) return INVALID_PLAYER_ID; // No passed text
		pos++;
	}
	new userid = INVALID_PLAYER_ID;
	if (IsNumeric(text[pos])) // Check whole passed string
	{
		// If they have a numeric name you have a problem (although names are checked on id failure)
		userid = strval(text[pos]);
		if (userid >=0 && userid < MAX_PLAYERS)
		{
			if(!IsPlayerConnected(userid))
			{
				/*if (playerid != INVALID_PLAYER_ID)
				{
					SendClientMessage(playerid, 0xFF0000AA, "User not connected");
				}*/
				userid = INVALID_PLAYER_ID;
			}
			else
			{
				return userid; // A player was found
			}
		}
		/*else
		{
			if (playerid != INVALID_PLAYER_ID)
			{
				SendClientMessage(playerid, 0xFF0000AA, "Invalid user ID");
			}
			userid = INVALID_PLAYER_ID;
		}
		return userid;*/
		// Removed for fallthrough code
	}
	// They entered [part of] a name or the id search failed (check names just incase)
	new len = strlen(text[pos]);
	new count = 0;
	new name[MAX_PLAYER_NAME];
	for (new i = 0; i < MAX_PLAYERS; i++)
	{
		if (IsPlayerConnected(i))
		{
			GetPlayerName(i, name, sizeof (name));
			if (strcmp(name, text[pos], true, len) == 0) // Check segment of name
			{
				if (len == strlen(name)) // Exact match
				{
					return i; // Return the exact player on an exact match
					// Otherwise if there are two players:
					// Me and MeYou any time you entered Me it would find both
					// And never be able to return just Me's id
				}
				else // Partial match
				{
					count++;
					userid = i;
				}
			}
		}
	}
	if (count != 1)
	{
		if (playerid != INVALID_PLAYER_ID)
		{
			if (count)
			{
				SendClientMessage(playerid, 0xFF0000AA, "Multiple users found, please narrow earch");
			}
			else
			{
				SendClientMessage(playerid, 0xFF0000AA, "No matching user found");
			}
		}
		userid = INVALID_PLAYER_ID;
	}
	return userid; // INVALID_USER_ID for bad return
}
IsNumeric(const string[])
{
	for (new i = 0, j = strlen(string); i < j; i++)
	{
		if (string[i] > '9' || string[i] < '0') return 0;
	}
	return 1;
}
strtok(string[],&idx,seperator = ' ')
{
	new ret[128], i = 0, len = strlen(string);
	while(string[idx] == seperator && idx < len) idx++;
	while(string[idx] != seperator && idx < len)
	{
	    ret[i] = string[idx];
	    i++;
		idx++;
	}
	while(string[idx] == seperator && idx < len) idx++;
	return ret;
}

stock strvalEx( const string[] )
{
	if( strlen( string ) >= 50 ) return 0; 
	return strval(string);
}

FormatNumber(number)
{
   new Str[15];
   format(Str, 15, "%d", number);

   if (strlen(Str) < sizeof(Str))
   {
      if (number >= 1000 && number < 10000)
             strins( Str, ",", 1, sizeof(Str));

      else if (number >= 10000 && number < 100000)
           strins(Str, ",", 2, sizeof(Str));

      else if (number >= 100000 && number < 1000000)
           strins(Str, ",", 3, sizeof(Str));

      else if (number >= 1000000 && number < 10000000)
           strins(Str, ",", 1, sizeof(Str)),strins(Str, ",", 5, sizeof(Str));

      else if (number >= 10000000 && number < 100000000)
           strins(Str, ",", 2, sizeof(Str)),strins(Str, ",", 6, sizeof(Str));

      else if (number >= 100000000 && number < 1000000000)
           strins(Str, ",", 3, sizeof(Str)),strins(Str, ",", 7, sizeof(Str));

      else if (number >= 1000000000 && number < 10000000000)
           strins(Str, ",", 1, sizeof(Str)),
           strins(Str, ",", 5, sizeof(Str)),
           strins(Str, ",", 9, sizeof(Str));
      else format(Str, 10, "%d", number);
   }
   else  format( Str, 15, "<BUG>" );
   return Str;
}

stock PhoneFormat(phonenumber)
{
	new number[16],negativ = 0, sep[2], tmp; sep = "-";
	if(phonenumber < 0) negativ = 1;
	format(number, sizeof(number), "%d", phonenumber);
	for(new i = strlen(number); i > negativ + 1; i--)
	{
		tmp++;
		if(tmp == 4)
		{
			strins(number, sep, i - 1); tmp  = 0; }
		}
	return number;
}